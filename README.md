#SiriusXM Radio Account Maker

##Download & Install
* Python 2.7 (https://www.python.org/downloads/)
* VirtualEnv (https://virtualenv.pypa.io/en/latest/installation.html)
* PIP (https://pip.pypa.io/en/latest/installing.html)

##Setup Bot
```
git clone git@bitbucket.org:rastof/siriusxm-account-maker.git
pip install -r requirements.txt
python sirius.py

```