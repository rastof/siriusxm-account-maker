import requests
from fng import FakeNameGenerator

person = FakeNameGenerator()
email = raw_input("full email: ")
form_data = {
  '_authkey_': 'null',
  'formId': '5',
  'evflag': '',
  'partnercode': 'sirius',
  'firstName': person.first_name(),
  'lastName': person.last_name(),
  'email': email,
  'emailConfirmation': email,
  'zip': person.zip(),
  'campaign': 'MYSXM_WEB30D',
  'f_currentsub': 'no',
  'agree-legal': 'Y',
}
form_headers = {
  'Host': 'www.siriusxm.com',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://www.siriusxm.com',
  'X-Requested-With': 'XMLHttpRequest',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  'Referer': 'https://www.siriusxm.com/SXM30DayOnlineTrial'
}
sirius = requests.Session()
form_response = sirius.post('https://www.siriusxm.com/p/formsubmit/FreeTrial', data=form_data, headers=form_headers, verify=False)

if form_response.status_code is 200:
    form = form_response.json()
  
    if form['formSubmitResponse']['status']['message'] == 'SUCCESS':
        print('success! check email')
    else:
        print('error! invalid email')
